#!/usr/bin/env perl6

use v6.c;

my $fnout = './kwrds.txt';

if ( !@*ARGS[0] ) {
    say "Usage: ./worstat-2-kwords.p6 text-file.txt"
} else {
    my $fnin = @*ARGS[0];
    my @keywords;
    if ( $fnin.IO.e ) {
        for $fnin.IO.slurp.lines -> $l {
            my $_l = $l;
            $_l ~~ s:g/ '+' //;
            $_l ~~ s:g/ <[\s\r\n\d]>* $//;
            @keywords.push($_l);
        }
        if (@keywords) {
            my $out = open $fnout, :w;
            $out.say(@keywords.join(q{,}));
            $out.close;
            say "DONE!";
        }
    } else {
        say "file <" ~ $fnin ~ "> not existed!"
    }
}
